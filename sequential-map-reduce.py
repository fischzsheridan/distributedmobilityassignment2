import csv
import time
from collections import Counter

filtered_words = ['the', 'but', 'and', 'with', 'but', 'a', 'an', 'or', 'to', 'in', 'at', 'because', 'is', 'of', 'on', 'for', 'you', 'be', 'will']
#takes a string and maps it to a dictionary that is a count of each word
def map_word_count(tweet): 
    word_count = {}
    words = tweet.split()
    for word in words:
        word = word.lower()
        if len(word) < 2 or word in filtered_words or '@' in word:
            continue
        if word not in word_count:
            word_count[word] = 0
        word_count[word] += 1
    return word_count

#merge word count dictionaries into a single dictionary
def reduce_word_counts(word_count1, word_count2):
    merged_dictionary = dict(Counter(word_count1) + Counter(word_count2))

    return merged_dictionary

# takes a list of tweets, maps each tweet to a dictionary, then reduces the list of dictionaries into a single one
def map_reduce_word_counts(tweets):
    word_counts = []
    for tweet in tweets:
        word_count = map_word_count(tweet)
        word_counts.append(word_count)
    
    reduced_word_counts = {}
    for word_count in word_counts:
        reduced_word_counts = reduce_word_counts(reduced_word_counts, word_count)

    return reduced_word_counts


#find highest value in a single dictionary
def reduce_highest(word_counts):
    highest = max(word_counts, key=word_counts.get)
    return highest

with open('Donald-Tweets!.csv') as tweet_file:
    start_time = time.time()
    reader = csv.DictReader(tweet_file)

    tweets = []
    for row in reader:
        tweets.append(row["Tweet_Text"])
    
    reduced_word_counts = map_reduce_word_counts(tweets)

    highest = reduce_highest(reduced_word_counts)

    f = open("sequential-map-reduce-OUTPUT.txt", "w")
    f.write('donald trumps favourite word is ' + highest + ' with ' + str(reduced_word_counts[highest]))
    f.write('\nthis script took ' + str(time.time() - start_time) + ' seconds to finish executing')

